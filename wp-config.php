<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'fotosvit_db');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$%}K)1 Q?u]LA#5lZB~@53Q[pG[_LAGtfb|]y6LPQ`g62nzBB^DtCy18#_t0U$.o');
define('SECURE_AUTH_KEY',  '(UZx%<`#Oa/#h=i<HEM~?/JL{2}_,yjL}gt]>sBBC&V+y6fTg(z$m}NF:CJ;|Q8{');
define('LOGGED_IN_KEY',    '4Je%+B1YMs`p0tx&[`TDQ$b_#cZ`XkWD^C`^bYdnNl^sqNvjAn=nr/bsV/ &[cjY');
define('NONCE_KEY',        'e6RU5T#|NXtBnPdie(}F:;6y&x5|0e#FvOIKBr=&0%mTgCh&cfQ]j18rf2n)$y@}');
define('AUTH_SALT',        'g[`:A?&J lKy9!JKBF=|e5>j -aX*^*Izl;(FZy?,(g?5g6OqC3y(oa;6=ngXzrg');
define('SECURE_AUTH_SALT', '&UFKRgn%OKs(RSR,>|;~CRfcSpH$p{^Y{Ob1]bO8r!VV3-B5qhB|)=Fn89Ma<[/H');
define('LOGGED_IN_SALT',   'F|=6N!p(Q=U)OQ@Db0XKB%8#j>IcNX^w<U*/l$wf}~($V[)eO{h[4Ykb!/q&#xxC');
define('NONCE_SALT',       'l_OFvxZ?Bm]5j]^KbJzPrxA@`>|,NzZJ?Po~O+#{*//S{;?5jt2{wK;REK-K0j-p');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
