<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php /* RSS и всякое */ ?>
    <link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
    <link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
    <link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="wrapper">
    <!-- Header Start -->
    <?php
        $phone1  = get_field('home_phone_1', 'option');
        $phone2  = get_field('home_phone_2', 'option');
    ?>
    <header class="header">
        <div class="page-container">
            <div class="nav__header-btn">
                <i></i><i></i><i></i>
                <span><?= __('Меню')?></span>
            </div>
            <div class="header__lang">
                <?php
                    wp_nav_menu(array(
                        'theme_location' => '',
                        'menu' => 'lang-menu',
                        'container' => 'nav',
                        'container_class' => '',
                        'container_id' => '',
                        'menu_class' => '',
                        'menu_id' => '',
                        'echo' => true,
                        'fallback_cb' => 'wp_page_menu',
                        'before' => '',
                        'after' => '',
                        'link_before' => '',
                        'link_after' => '',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 0,
                        'walker' => '',
                    ));
	            ?>
            </div>
            <div class="header__logo">
                <img src="<?php echo get_template_directory_uri();?>/images/header-logo.png" alt="">
	            <div class="header__address"><?= __('м.Вінниця, вул.Зодчих 3') ?></div>
            </div>
            <nav class="nav__header">
		        <?php
		        wp_nav_menu(array(
			        'theme_location' => '',
			        'menu' => 'header-menu',
			        'container' => '',
			        'container_class' => '',
			        'container_id' => '',
			        'menu_class' => '',
			        'menu_id' => '',
			        'echo' => true,
			        'fallback_cb' => 'wp_page_menu',
			        'before' => '',
			        'after' => '',
			        'link_before' => '',
			        'link_after' => '',
			        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			        'depth' => 0,
			        'walker' => '',
		        ));
		        ?>
            </nav>
            <div class="header__info">
                <?php if($phone1){?><div class="phone__box"><a href="tel:<?= $phone1 ?>"><?= $phone1 ?></a></div><?php } ?>
	            <?php if($phone2){?><div class="phone__box"><a href="tel:<?= $phone2 ?>"><?= $phone2 ?></a></div><?php } ?>
                <div class="link__action"><a rel="nofollow" href="#" data-toggle="modal" data-target="#modal-callback"><?= __('Замовити зворотній зв’язок') ?></a></div>
            </div>
        </div>
    </header>
    <!-- Header End -->