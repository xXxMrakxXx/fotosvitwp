$(document).ready(function () {
    // Init fancyBox
    $().fancybox({
        selector : '.slider__portfolio .slick-slide:not(.slick-cloned) a',
        backFocus: false
    });

    // init slick slider
    $('.slider__portfolio').slick({
        centerMode: true,
        arrows: true,
        prevArrow: '<div class="slick__arrow slick__arrow--left"><i class="fas fa-chevron-circle-left"></i></div>',
        nextArrow: '<div class="slick__arrow slick__arrow--right"><i class="fas fa-chevron-circle-right"></i></div>',
        dots: false,
        infinite: true,
        draggable: false,
        pauseOnFocus: true,
        pauseOnHover: true,
        centerPadding: '0',
        slidesToShow: 5,
        lazyLoad: 'ondemand',
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    draggable: true,
                    centerMode: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    if($(window).width() < 1024){
        $('.slider__portfolio').on('click', '.slick-cloned', function(e) {
            var $slides = $(this)
                .parent()
                .children('.slick-slide:not(.slick-cloned) a');

            $slides
                .eq( ( $(this).attr("data-slick-index") || 0) % $slides.length )
                .trigger("click.fb-start", { $trigger: $(this) });

            return false;
        });
    }
    // init slick slider
    $('.slider__reviews').slick({
        centerMode: true,
        arrows: false,
        dots: true,
        infinite: true,
        pauseOnFocus: true,
        pauseOnHover: true,
        centerPadding: '0',
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    draggable: true,
                    centerMode: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    // nav anchor
    $('.nav__header li').on('click', 'a', function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top - 40;

        $('body, html').animate({scrollTop: top}, 1500);
    });

    // mobile menu button
    $('.nav__header-btn').click(function () {
       $('.nav__header-btn').toggleClass('active');
       $('.nav__header ul').toggle('slow').toggleClass('active');
    });

    // phone input mask
    $("input[type=tel]").mask("+380 ( 99 ) 999 99 99");

    //fixed menu
    var menuHeight = $('.header').innerHeight();

    if($(window).scrollTop() > menuHeight){
        $('.header').addClass('header-fixed');
    }
    $(window).scroll(function(){
        if($(this).scrollTop() > menuHeight){
            $('.header').addClass('header-fixed');
        }
        else if ($(this).scrollTop() < menuHeight){
            $('.header').removeClass('header-fixed');
        }
    });

});

//youtube load image, on click change on iframe
( function() {

    var youtube = document.querySelectorAll( ".youtube" );

    for (var i = 0; i < youtube.length; i++) {

        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";

        var image = new Image();
        image.src = source;
        image.addEventListener( "load", function() {
            youtube[ i ].appendChild( image );
        }( i ) );

        youtube[i].addEventListener( "click", function() {

            var iframe = document.createElement( "iframe" );

            iframe.setAttribute( "frameborder", "0" );
            iframe.setAttribute( "allowfullscreen", "" );
            iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

            this.innerHTML = "";
            this.appendChild( iframe );

            if(iframe){
                $('.slider__reviews').slick('slickPause');
            }

            $('.slider__reviews iframe').mouseover(function() {
                $('.slider__reviews').slick('slickPause');
            }).mouseout(function() {
                $('.slider__reviews').slick('slickPlay');
            });
        } );
    }

} )();