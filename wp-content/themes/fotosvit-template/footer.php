<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 */
?>
    <?php
        $address = get_field('home_address', 'option');
        $phone1  = get_field('home_phone_1', 'option');
        $phone2  = get_field('home_phone_2', 'option');
        $mail    = get_field('home_mail', 'option');

        $twitter = get_field('twitter_link', 'option');
        $facebook = get_field('facebook_link', 'option');
        $instagram = get_field('instagram_link', 'option');
    ?>
    <!-- Footer Start -->
    <footer id="nav-contacts" class="footer section__wrap--gradient">
        <div class="page-container">
            <div class="footer__info">
                <h2 class="section__title section__title--light"><?= __('Контакти') ?></h2>
	            <div class="footer__address"><?= __('м.Вінниця, вул.Зодчих 3') ?></div>
	            <?php if($phone1){?><div class="phone__box"><a href="tel:<?= $phone1 ?>"><?= $phone1 ?></a></div><?php } ?>
	            <?php if($phone2){?><div class="phone__box"><a href="tel:<?= $phone2 ?>"><?= $phone2 ?></a></div><?php } ?>
	            <?php if($mail){?><div class="mail__box"><a href="mailto:<?= $mail ?>"><?= $mail ?></a></div><?php } ?>
            </div>
            <div class="footer__map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1265.9231210013613!2d28.449751696229043!3d49.21886468559958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xee178ae7604ce3bd!2sFotoSvit!5e0!3m2!1sru!2sua!4v1538935778169" width="100%" height="235" frameborder="0" style="border:0" allowfullscreen></iframe>
                <div class="footer__box">
                    <div class="footer__rights"><u><?= __('Усі права захищені') ?> Fotosvit <?= date("Y") ?></u></div>
                    <div class="social__box">
                        <ul>
	                        <?php if($facebook){?><li><a target="_blank" href="<?= $facebook ?>"><i class="fab fa-facebook-f"></i></a></li><?php } ?>
	                        <?php if($instagram){?><li><a target="_blank" href="<?= $instagram ?>"><i class="fab fa-instagram"></i></a></li><?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer End -->

    <!-- Modal Start -->
    <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?= __('Залишіть заявку') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
	                <?= do_shortcode('[cf7-form cf7key="contact-form-uk"]') ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->
</div>
<script src="<?php echo get_template_directory_uri();?>/vendor/jquery/jquery.min.js"></script>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
</body>
</html>