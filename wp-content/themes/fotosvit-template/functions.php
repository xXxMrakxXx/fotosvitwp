<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 */

add_theme_support('title-tag'); // теперь тайтл управляется самим вп

register_nav_menus(array( // Регистрируем 2 меню
	'top' => 'Верхнее', // Верхнее
));

add_theme_support('post-thumbnails'); // включаем поддержку миниатюр
set_post_thumbnail_size(250, 150); // задаем размер миниатюрам 250x150
add_image_size('big-thumb', 400, 400, true); // добавляем еще один размер картинкам 400x400 с обрезкой

// De-register jQuery from Contact Form 7
/*add_action('wp_print_scripts', 'my_deregister_javascript', 100 );
function my_deregister_javascript() {
	wp_deregister_script( 'contact-form-7' );
}*/


add_action('wp_footer', 'add_scripts'); // приклеем ф-ю на добавление скриптов в футер
if (!function_exists('add_scripts')) { // если ф-я уже есть в дочерней теме - нам не надо её определять
	function add_scripts() { // добавление скриптов
	    if(is_admin()) return false; // если мы в админке - ничего не делаем
	    //wp_deregister_script('jquery'); // выключаем стандартный jquery
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery',get_template_directory_uri().'/vendor/jquery/jquery.min.js','','',true);

		wp_enqueue_script('bootstrap', get_template_directory_uri().'/vendor/bootstrap/js/bootstrap.bundle.min.js','','',true);

		wp_enqueue_script('fancybox', get_template_directory_uri().'/vendor/fancybox/jquery.fancybox.min.js','','',true);
		wp_enqueue_script('slick', get_template_directory_uri().'/vendor/slick/slick.js','','',true);
		wp_enqueue_script('maskedinput', get_template_directory_uri().'/vendor/jquery-maskedinput/jquery.maskedinput.min.js','','',true);

		wp_enqueue_script('main', get_template_directory_uri().'/js/main.min.js','','',true);
	}
}

function add_defer_attribute($tag, $handle) {
	// add script handles to the array below
	$scripts_to_defer = array(
		'bootstrap',
		'fancybox',
		'slick',
		'maskedinput',
		'main'
	);

	foreach($scripts_to_defer as $defer_script) {
		if ($defer_script === $handle) {
			return str_replace(' src', ' defer="defer" src', $tag);
		}
	}
	return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

add_action('wp_print_styles', 'add_styles'); // приклеем ф-ю на добавление стилей в хедер
if (!function_exists('add_styles')) { // если ф-я уже есть в дочерней теме - нам не надо её определять
	function add_styles() { // добавление стилей
	    if(is_admin()) return false; // если мы в админке - ничего не делаем
	    wp_enqueue_style( 'critical', get_template_directory_uri().'/css/critical.css' );
	}
}
// подключаем стили в подвал сайта
function prefix_add_footer_styles() {
	wp_enqueue_style( 'main', get_template_directory_uri(). '/css/main.css' );
};
add_action( 'get_footer', 'prefix_add_footer_styles' );


// создаем кастомное меню в админке
if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' => 'Общие настройки темы',
		'menu_title' => 'Настройки темы',
		'menu_slug' => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect' => false
	));
}