<?php
/**
 * Template Name: Home template
 */
?>

<?php get_header(); ?>

<!-- Home Content Start -->
<?php
    $home_banner_photo = get_field('home_banner_photo');
    $home_banner_text  = get_field('home_banner_text');
?>
<section id="nav-main" class="section__wrap home__main">
    <div class="page-container">
        <div class="section__box--left">
            <h2 class="section__title section__title--dark"><span><?= __('Друк <br> поліграфії') ?></span></h2>
            <div class="section__text"><?= $home_banner_text ?></div>
            <div class="btn__base btn__base--green"><a rel="nofollow" href="#" data-toggle="modal" data-target="#modal-callback"><?= __('Безкоштовна консультація') ?></a></div>
        </div>
        <div class="section__box--right">
            <div class="img__box"><img src="<?= $home_banner_photo['url']; ?>" alt="<?= $home_banner_photo['alt']; ?>"></div>
        </div>
    </div>
</section>
<section class="section__wrap home__main-description">
    <div class="page-container">
        <div class="section__box--left">
            <div class="home__description-title">
	            <?= __('10 років досвіду друку поліграфії') ?>
            </div>
        </div>
        <div class="section__box--right">
            <div class="section__text">
	            <?= __('Друкуємо візитки, флаєра, листівки, буклети, брошури, блокноти, книги, бланки, наклейки, банери, бланки, конверти, меню та іншу поліграфічну продукцію') ?>
            </div>
        </div>
    </div>
</section>
<section class="section__wrap section__wrap--gradient home__about">
    <div class="page-container">
        <div class="section__box--left">
            <div class="img__box"><img src="<?php echo get_template_directory_uri();?>/images/about1-1.png" alt=""></div>
        </div>
        <div class="section__box--right">
            <div class="items__wrap">
                <div class="item__box">
                    <div class="item__icon"><img src="<?php echo get_template_directory_uri();?>/images/about1.png" alt=""></div>
                    <div class="item__text"><?= __('Друкуємо скільки потрібно') ?></div>
                </div>
                <div class="item__box">
                    <div class="item__icon"><img src="<?php echo get_template_directory_uri();?>/images/about2.png" alt=""></div>
                    <div class="item__text"><?= __('Вигідні ціни') ?></div>
                </div>
                <div class="item__box">
                    <div class="item__icon"><img src="<?php echo get_template_directory_uri();?>/images/about3.png" alt=""></div>
                    <div class="item__text"><?= __('Найновіше обладнання') ?></div>
                </div>
                <div class="item__box">
                    <div class="item__icon"><img src="<?php echo get_template_directory_uri();?>/images/about4.png" alt=""></div>
                    <div class="item__text"><?= __('Послуги дизайнера та менеджера') ?></div>
                </div>
                <div class="item__box">
                    <div class="item__icon"><img src="<?php echo get_template_directory_uri();?>/images/about5.png" alt=""></div>
                    <div class="item__text"><?= __('Друкуємо вчасно') ?></div>
                </div>
                <div class="item__box">
                    <div class="item__icon"><img src="<?php echo get_template_directory_uri();?>/images/about6.png" alt=""></div>
                    <div class="item__text"><?= __('Широкий вибір матеріалу') ?></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section__wrap home__calculate">
    <div class="page-container">
        <div class="section__box--left">
            <h2 class="section__title section__title--dark"><?= __('<span>Прорахуйте</span> вартість з менеджером') ?></h2>
            <div class="section__text">
	            <?= __('Наш менедежр зв’яжеться з вами, ви зможете прорахувати суму замовлення та отримати <span>БЕЗКОШТОВНУ</span> консультацію') ?>
            </div>
            <div class="btn__base btn__base--green"><a rel="nofollow" href="#" data-toggle="modal" data-target="#modal-callback"><?= __('Прорахувати') ?></a></div>
        </div>
        <div class="section__box--right">
            <?php $home_manager  = get_field('manager_photo');?>
            <div class="img__box"><img src="<?= $home_manager['url']; ?>" alt="<?= $home_manager['alt']; ?>"></div>
        </div>
    </div>
</section>
<section id="nav-services" class="section__wrap home__print">
    <div class="page-container">
        <h2 class="section__title section__title--dark"><?= __('Також ми друкуємо') ?></h2>
        <?php if( have_rows('home_services') ): ?>
            <div class="items__wrap">
                <?php $i = 0; while( have_rows('home_services') ): the_row();
                    $image = get_sub_field('icon');
                    $text  = get_sub_field('text');
                    $price = get_sub_field('price');
                    ?>
                    <div class="item__box">
                        <div class="item__box--inside">
                        <?php if($price){?><a rel="nofollow" data-fancybox="services_<?= $i ?>" href="<?= $price['url']; ?>"><?php } ?>
                            <div class="item__icon"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></div>
                            <div class="item__text"><?= $text; ?></div>
                        <?php if($price){?></a><?php } ?>
                        </div>
                    </div>
                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>
<section class="section__wrap section__wrap--gradient home__steps">
    <div class="page-container">
        <div class="section__box--left">
            <?php $homeStepsPhoto = get_field('home-steps-photo'); ?>
            <div class="img__box"><img src="<?= $homeStepsPhoto['url']; ?>" alt="<?= $homeStepsPhoto['alt']; ?>"></div>
            <div class="btn__base btn__base--green"><a rel="nofollow" href="#" data-toggle="modal" data-target="#modal-callback"><?= __('Залишити заявку') ?></a></div>
        </div>
        <div class="section__box--right">
            <h2 class="section__title section__title--light"><?= __('Просто<br> залиште заявку на сайті') ?></h2>
	        <?php if( have_rows('home-steps') ): ?>
                <div class="items__wrap">
			        <?php $i = 1; while( have_rows('home-steps') ): the_row();
				        $text = get_sub_field('text');
				        ?>
                        <div class="item__box">
                            <span class="number__box"><?= $i ?></span>
                            <?= $text ?>
                        </div>
			        <?php $i++; endwhile; ?>
                </div>
	        <?php endif; ?>
        </div>
    </div>
</section>
<section class="section__wrap home__companies">
    <div class="page-container">
        <h2 class="section__title section__title--dark"><?= __('Компанії, які нам <span>довіряють</span>')?></h2>
	    <?php if( have_rows('home_companies') ): ?>
            <div class="items__wrap">
			    <?php while( have_rows('home_companies') ): the_row();
				    $image1 = get_sub_field('logo');
				    $image2 = get_sub_field('example');
				    ?>
                    <div class="item__box">
                        <div class="item__icon"><img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt'] ?>" /></div>
                        <?php if($image2){ ?><div class="item__overlay"><span><img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt'] ?>" /></span></div><?php }?>
                    </div>
			    <?php endwhile; ?>
            </div>
	    <?php endif; ?>
    </div>
</section>
<section id="nav-portfolio" class="section__wrap home__portfolio">
    <div class="page-container">
        <h2 class="section__title section__title--dark"><?= __('Портфоліо компанії <span>Fotosvit</span>')?></h2>
	    <?php if( have_rows('home_portfolio') ): ?>
            <div class="slider__wrap slider__portfolio">
			    <?php while( have_rows('home_portfolio') ): the_row();
				    $image = get_sub_field('icon');
				    ?>
                    <div class="slider__item">
                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
                        <a rel="nofollow" data-fancybox="portfolio" href="<?= $image['url']; ?>">
                            <i class="fas fa-search-plus"></i>
                        </a>
                    </div>
			    <?php endwhile; ?>
            </div>
	    <?php endif; ?>
    </div>
</section>
<section id="nav-about" class="section__wrap home__main-description">
    <div class="page-container">
	    <?php if( have_rows('text_repeater') ): ?>
            <div class="items__wrap">
			    <?php while( have_rows('text_repeater') ): the_row();
				    $title    = get_sub_field('title');
				    $subtitle = get_sub_field('subtitle');
				    $text     = get_sub_field('text');
				    ?>
                    <div class="item__box">
                        <div class="item__title"><?= $title?></div>
                        <div class="item__sub-title"><?= $subtitle?></div>
                        <div class="item__text"><?= $text?></div>
                    </div>
				    <?php endwhile; ?>
            </div>
	    <?php endif; ?>
    </div>
</section>
<section class="section__wrap home__reviews">
    <div class="page-container">
        <h2 class="section__title section__title--dark"><?= __('<span>Відгуки наших</span> клієнтів')?></h2>
	    <?php if( have_rows('home_reviews') ): ?>
            <div class="slider__wrap slider__reviews">
			    <?php while( have_rows('home_reviews') ): the_row();
				    $text = get_sub_field('text');
				    ?>
                    <div class="slider__item">
                        <div class="youtube" data-embed="<?= $text; ?>">
                            <div class="play-button"><i class="fab fa-youtube"></i></div>
                        </div>
                    </div>
			    <?php endwhile; ?>
            </div>
	    <?php endif; ?>
    </div>
</section>
<!-- Home Content End -->

<?php get_footer(); ?>